﻿module straw.core.cfx;

import std.ascii;
import std.algorithm;
import std.conv;
import std.exception;
import std.utf;
import std.range;
import straw.core.exception;
import straw.core.storage;
import std.file;
import std.container.array;

alias CfxParserException = DerivedStrawException!( "CfxParserException", CfxParser );
alias CfxException = DerivedStrawException!( "CfxException", CfxElement );

__gshared CfxEmptyValue cfxEmptyValue = new CfxEmptyValue();

abstract class CfxElement {

public:
	alias Type = CfxElementType;

public:
	CfxElement opIndex( Arg1, Args ... )( Arg1 arg1, Args args ) {
		static if( Args.length )
			return __opIndex( arg1 ).opIndex( args );

		else
			return __opIndex( arg1 );
	}
	void opIndexAssign( T, Arg1, Args ... )( T set, Arg1 arg1, Args args ) {
		static if( Args.length )
			__opIndex( arg1 )[ args ] = set;
		
		else
			__opIndex( arg1 ) = set;
	}

	CfxElement* opBinaryRight( string s : "in" )( string fieldName ) {
		return __opBinaryRight_in( fieldName );
	}

	int opApply( int delegate( ref CfxElement ) block ) {
		throw new CfxException( "notAnArrayOrObject", this );
	}
	int opApply( int delegate( string, ref CfxElement ) block ) {
		throw new CfxException( "notAnObject", this );
	}

protected:
	/// Works for CfxObject, otherwise throws CfxException
	CfxElement __opIndex( string fieldName ) {
		throw new CfxException( "notAnObject", this );
	}
	/// Works for CfxObject, otherwise throws CfxException
	void __opIndexAssign( CfxElement set, string fieldName ) {
		throw new CfxException( "notAnObject", this );
	}

	/// Works for CfxField and CfxObject, otherwise throws CfxException
	CfxElement __opIndex( size_t index ) {
		throw new CfxException( "notAnObjectOrField", this );
	}
	/// Works for CfxField and CfxObject, otherwise throws CfxException
	void __opIndexAssign( CfxElement set, size_t index ) {
		throw new CfxException( "notAnObjectOrField", this );
	}

	CfxElement* __opBinaryRight_in( string fieldName ) {
		throw new CfxException( "notAnObject", this );
	}

public:
	final ref string toStr() {
		if( auto val = cast( CfxValue ) this )
			return val.value;

		throw new CfxException( "wrongElementType", this, "requiredType", Type.value );
	}
	final CfxArray toArray() {
		if( auto val = cast( CfxArray ) this )
			return val;
		
		throw new CfxException( "wrongElementType", this, "requiredType", Type.array );
	}
	final CfxObject toObj() {
		if( auto val = cast( CfxObject ) this )
			return val;
		
		throw new CfxException( "wrongElementType", this, "requiredType", Type.object );
	}

public:
	final bool toBool() {
		return toStr == "true";
	}

public:
	abstract Type type();

protected:
	abstract string toStringImpl( size_t offset );
	final string offsetStr( size_t offset ) {
		return ( cast(immutable(char)) ' ' ).repeat( offset * 2 ).array;
	}

public:
	final string[ string ] __exceptionInfo() {
		return [
			"address": ( cast( void* ) this ).to!string,
			"type": type.to!string
		];
	}

public:
	static CfxObject createFromFile( string filename ) {
		return CfxParser( filename.readText.replace( "\r\n", "\n" ), filename ).parseRoot();
	}

}

enum CfxElementType {
	object,
	array,
	value,
	empty
}

final class CfxEmptyValue : CfxElement {

public:
	override Type type() {
		return Type.empty;
	}

protected:
	override string toStringImpl( size_t offset ) {
		return "";
	}

}

final class CfxObject : CfxElement {

public:
	CfxElement[ string ] fields;

protected:
	/// Works for CfxObject, otherwise throws CfxException
	override CfxElement __opIndex( string fieldName ) {
		if( auto it = fieldName in fields )
			return *it;
		else
			return cfxEmptyValue;
	}
	/// Works for CfxObject, otherwise throws CfxException
	override void __opIndexAssign( CfxElement set, string fieldName ) {
		fields[ fieldName ] = set;
	}

	/// Works for CfxField and CfxObject, otherwise throws CfxException
	override CfxElement __opIndex( size_t index ) {
		auto keys = fields.keys;

		if( index < keys.length )
			return fields[ keys[ index ] ];
		else
			return cfxEmptyValue;
	}
	/// Works for CfxField and CfxObject, otherwise throws CfxException
	override void __opIndexAssign( CfxElement set, size_t index ) {
		auto keys = fields.keys;

		if( index < keys.length )
			fields[ keys[ index ] ] = set;
		else
			throw new CfxException( "indexOutsideRange", this, "index", index );
	}

	override CfxElement* __opBinaryRight_in( string fieldName ) {
		return fieldName in fields;
	}

public:
	override int opApply( int delegate( ref CfxElement ) block ) {
		foreach( key, ref item; fields ) {
			if( int result = block( item ) )
				return result;
		}
		return 0;
	}
	override int opApply( int delegate( string, ref CfxElement ) block ) {
		foreach( key, ref item; fields ) {
			if( int result = block( key, item ) )
				return result;
		}
		return 0;
	}

public:
	override Type type() {
		return Type.object;
	}

protected:
	override string toStringImpl( size_t offset ) {
		return "{\n" ~ toStringInner( offset ) ~ offsetStr( offset - 1 ) ~ "}";
	}
	string toStringInner( size_t offset ) {
		string result;
		foreach( key, value; fields )
			result ~= offsetStr( offset ) ~ key ~ ": " ~ value.toStringImpl( offset + 1 ) ~ '\n';
		return result;
	}

public:
	override string toString() {
		return toStringInner( 0 );
	}

}

final class CfxValue : CfxElement {

public:
	string value;

public:
	this( string value ) {
		this.value = value;
	}

public:
	override Type type() {
		return Type.value;
	}

protected:
	override string toStringImpl( size_t offset ) {
		return value;
	}

}

final class CfxArray : CfxElement {

public:
	Array!CfxElement items;

protected:
	/// Works for CfxField and CfxObject, otherwise throws CfxException
	override CfxElement __opIndex( size_t index ) {
		if( index < items.length )
			return items[ index ];
		else
			return cfxEmptyValue;
	}
	override /// Works for CfxField and CfxObject, otherwise throws CfxException
	void __opIndexAssign( CfxElement set, size_t index ) {
		if( index < items.length )
			items[ index ] = set;
		else
			throw new CfxException( "indexOutsideRange", this, "index", index );
	}

public:
	override int opApply( int delegate( ref CfxElement ) dg ) {
		foreach( ref item; items ) {
			if( int result = dg( item ) )
				return result;
		}
		return 0;
	}

public:
	override Type type() {
		return Type.array;
	}

protected:
	override string toStringImpl( size_t offset ) {
		return "[ " ~ items[].map!( x => x.toStringImpl( offset ) ).join( ", " ).array.toUTF8 ~ " ]";
	}

}

private struct CfxParser {

private:
	string source, sourceAddr;
	size_t pos, line, linePos;
	char current;

public:
	@disable this();
	this( string source, string sourceAddr ) {
		this.source = source;
		this.sourceAddr = sourceAddr;

		pos = 0;
		line = 1;
		linePos = 1;

		if( source.length )
			current = source[ 0 ];
	}

public:
	char next() {
		if( pos >= source.length - 1 ) {
			current = 0;
			return 0;
		}

		pos ++;
		current = source[ pos ];
		linePos ++;

		if( current == '\n' ) {
			line ++;
			linePos = 1;
		}

		return current;
	}

public:
	void skipWhitespaces() {
		while( current == ' ' || current == '\t' ) next();
	}
	void skipWhitespacesAndNewlines() {
		while( current == ' ' || current == '\n' || current == '\t' ) next();
	}

	char expectAndNext( char symbol ) {
		enforce( current == symbol, new CfxParserException( "unexpectedSymbol", this, "expected", symbol ) );
		return next();
	}
	char expectNewlineAndNext() {
		return expectAndNext( '\n' );
	}

public:
	CfxObject parseRoot() {
		return parseObject( true );
	}
	CfxObject parseObject( bool withoutBrackets = false ) {
		if( !withoutBrackets )
			expectAndNext( '{' );

		CfxObject result = new CfxObject();

		skipWhitespacesAndNewlines();
		if( current == 0 || ( !withoutBrackets && current == '}' ) ) {
			if( !withoutBrackets && current == 0 )
				throw new CfxParserException( "Unexpected EOF", this );

			next();
			return result;
		}

		while( true ) {
			string fieldName = parseFieldName();
			expectAndNext( ':' ), skipWhitespaces();
			CfxElement fieldValue = parseElement();

			enforce( fieldName !in result.fields, new CfxParserException( "fieldAlreadyExists", this, "fieldName", fieldName ) );
			result.fields[ fieldName ] = fieldValue;

			skipWhitespaces();

			bool isSeparator = false;
			if( current == ',' ) {
				next();
				skipWhitespaces();
				isSeparator = true;
			}
			if( current == '\n' ) {
				next();
				skipWhitespacesAndNewlines();
				isSeparator = true;
			}

			if( ( withoutBrackets && current == 0 ) || ( !withoutBrackets && current == '}' ) )
				break;

			if( !isSeparator )
				throw new CfxParserException( "expectedNewlineOrComma", this );
		}

		// EOF or }
		next();

		return result;
	}
	CfxElement parseElement() {
		if( current.isAlphaNum )
			return cast( CfxElement ) parseValue();

		else if( current == '"' )
			return cast( CfxElement ) parseQuotedValue();

		else if( current == '{' )
			return cast( CfxElement ) parseObject();

		else if( current == '[' )
			return cast( CfxElement ) parseArray();

		else
			throw new CfxParserException( "elementNotRecognized", this );
	}
	CfxValue parseValue() {
		size_t startPos = pos, endPos = pos;

	cycle:
		while( true ) {
			switch( current ) {

				case '\n', ',', ']', '}', 0:
					break cycle;

				case ' ', '\t':
					next();
					break;

				default:
					next();
					endPos = pos;
					break;

			}
		}
		return new CfxValue( source[ startPos .. endPos ] );
	}
	CfxValue parseQuotedValue() {
		expectAndNext( '"' );
		size_t startPos = pos;

		while( current != '"' ) {
			if( current == 0 )
				throw new CfxParserException( "expectedQuotesEndNotEOF", this );

			next();
		}

		expectAndNext( '"' );

		return new CfxValue( source[ startPos .. pos - 1 ] );
	}
	CfxArray parseArray() {
		expectAndNext( '[' );

		CfxArray result = new CfxArray();
		while( true ) {
			skipWhitespacesAndNewlines();

			if( current == ']' )
				break;

			result.items ~= parseElement();
			skipWhitespaces();

			if( ( current != ',' && current != '\n' ) || current == ']' )
				break;

			next();
		}

		expectAndNext( ']' );

		return result;
	}

public:
	string parseFieldName() {
		size_t startPos = pos;

	loop:
		while( true ) {
			switch( current ) {

				case ':':
					break loop;

				case '\n', 0, '{', '[', ']', '}':
					throw new CfxParserException( "unexpectedSymbol", this, "expected", "fieldNameSymbol" );

				default:
					next();
			}
		}
		enforce( pos != startPos, new CfxParserException( "expectedFieldName", this ) );
		return source[ startPos .. pos ];
	}

public:
	string[ string ] __exceptionInfo() {
		string ch;
		switch( current ) {

			case '\n':
				ch = "\n";
				break;

			case '\t':
				ch = "\t";
				break;

			case 0:
				ch = "EOF";
				break;

			default:
				ch = current.to!string;
				break;

		}

		return [
			"line": line.to!string,
			"pos": linePos.to!string,
			"currentCharacter": ch,
			"source": sourceAddr
		];
	}

}