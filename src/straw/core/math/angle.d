﻿module straw.core.math.angle;

import std.math;
import straw.core.object;

/// Stores angle in 0.0-1.0 format
struct Angle {
	mixin( StrawObject );
	
private:
	float sin_, cos_;
	
public:
	alias T = float;
	T data = 0;
	alias data this;
	
public:
	this( T data ) {
		this.data = data;
	}
	
public:
	double sinOf() {
		if( sin_.isNaN )
			sin_ = sin( data * 2 * PI );
		
		return sin_;
	}
	double cosOf() {
		if( cos_.isNaN )
			cos_ = cos( data * 2 * PI );
		
		return cos_;
	}
	
}