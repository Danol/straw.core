﻿module straw.core.math.viewMatrix;

import straw.core.math.angle;
import straw.core.math.vector;
import straw.core.object;

struct ViewMatrix {
	mixin( StrawObject );

public:
	alias T = float;
	alias V2T = V2F;

	enum : ubyte {
		xx, yx, zx, wx,
		xy, yy, zy, wy,
		xz, yz, zz, wz,
		xw, yw, zw, ww
	}
	
public:
	T[ 16 ] m = [
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	];
	
public:
	this( T[ 16 ] m ... ) {
		this.m = m;
	}
	
public:
	This apply( This mat ) {
		This tmp = [ 0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0 ];
		
		foreach( x; 0 .. 4 ) {
			foreach( y; 0 .. 4 ) {
				
				foreach( progress; 0 .. 4 ) {
					tmp.m[ x * 4 + y ] += m[ progress * 4 + y ] * mat.m[ x * 4 + progress ];
				}
				
			}
		}
		
		return tmp;
	}
	
	V2T apply( V2T v ) {
		V2T ret;
		
		ret.x = v.x * m[ xx ] + v.y * m[ xy ] + m[ xw ];
		ret.y = v.x * m[ yx ] + v.y * m[ yy ] + m[ yw ];
		
		return ret;
	}
	
	This invert() {
		/* stolen from MESA */
		This inv;
		float det;
		
		inv.m[0] = m[5]  * m[10] * m[15] - 
			m[5]  * m[11] * m[14] - 
			m[9]  * m[6]  * m[15] + 
			m[9]  * m[7]  * m[14] +
			m[13] * m[6]  * m[11] - 
			m[13] * m[7]  * m[10];
		
		inv.m[4] = -m[4]  * m[10] * m[15] + 
			m[4]  * m[11] * m[14] + 
			m[8]  * m[6]  * m[15] - 
			m[8]  * m[7]  * m[14] - 
			m[12] * m[6]  * m[11] + 
			m[12] * m[7]  * m[10];
		
		inv.m[8] = m[4]  * m[9] * m[15] - 
			m[4]  * m[11] * m[13] - 
			m[8]  * m[5] * m[15] + 
			m[8]  * m[7] * m[13] + 
			m[12] * m[5] * m[11] - 
			m[12] * m[7] * m[9];
		
		inv.m[12] = -m[4]  * m[9] * m[14] + 
			m[4]  * m[10] * m[13] +
			m[8]  * m[5] * m[14] - 
			m[8]  * m[6] * m[13] - 
			m[12] * m[5] * m[10] + 
			m[12] * m[6] * m[9];
		
		inv.m[1] = -m[1]  * m[10] * m[15] + 
			m[1]  * m[11] * m[14] + 
			m[9]  * m[2] * m[15] - 
			m[9]  * m[3] * m[14] - 
			m[13] * m[2] * m[11] + 
			m[13] * m[3] * m[10];
		
		inv.m[5] = m[0]  * m[10] * m[15] - 
			m[0]  * m[11] * m[14] - 
			m[8]  * m[2] * m[15] + 
			m[8]  * m[3] * m[14] + 
			m[12] * m[2] * m[11] - 
			m[12] * m[3] * m[10];
		
		inv.m[9] = -m[0]  * m[9] * m[15] + 
			m[0]  * m[11] * m[13] + 
			m[8]  * m[1] * m[15] - 
			m[8]  * m[3] * m[13] - 
			m[12] * m[1] * m[11] + 
			m[12] * m[3] * m[9];
		
		inv.m[13] = m[0]  * m[9] * m[14] - 
			m[0]  * m[10] * m[13] - 
			m[8]  * m[1] * m[14] + 
			m[8]  * m[2] * m[13] + 
			m[12] * m[1] * m[10] - 
			m[12] * m[2] * m[9];
		
		inv.m[2] = m[1]  * m[6] * m[15] - 
			m[1]  * m[7] * m[14] - 
			m[5]  * m[2] * m[15] + 
			m[5]  * m[3] * m[14] + 
			m[13] * m[2] * m[7] - 
			m[13] * m[3] * m[6];
		
		inv.m[6] = -m[0]  * m[6] * m[15] + 
			m[0]  * m[7] * m[14] + 
			m[4]  * m[2] * m[15] - 
			m[4]  * m[3] * m[14] - 
			m[12] * m[2] * m[7] + 
			m[12] * m[3] * m[6];
		
		inv.m[10] = m[0]  * m[5] * m[15] - 
			m[0]  * m[7] * m[13] - 
			m[4]  * m[1] * m[15] + 
			m[4]  * m[3] * m[13] + 
			m[12] * m[1] * m[7] - 
			m[12] * m[3] * m[5];
		
		inv.m[14] = -m[0]  * m[5] * m[14] + 
			m[0]  * m[6] * m[13] + 
			m[4]  * m[1] * m[14] - 
			m[4]  * m[2] * m[13] - 
			m[12] * m[1] * m[6] + 
			m[12] * m[2] * m[5];
		
		inv.m[3] = -m[1] * m[6] * m[11] + 
			m[1] * m[7] * m[10] + 
			m[5] * m[2] * m[11] - 
			m[5] * m[3] * m[10] - 
			m[9] * m[2] * m[7] + 
			m[9] * m[3] * m[6];
		
		inv.m[7] = m[0] * m[6] * m[11] - 
			m[0] * m[7] * m[10] - 
			m[4] * m[2] * m[11] + 
			m[4] * m[3] * m[10] + 
			m[8] * m[2] * m[7] - 
			m[8] * m[3] * m[6];
		
		inv.m[11] = -m[0] * m[5] * m[11] + 
			m[0] * m[7] * m[9] + 
			m[4] * m[1] * m[11] - 
			m[4] * m[3] * m[9] - 
			m[8] * m[1] * m[7] + 
			m[8] * m[3] * m[5];
		
		inv.m[15] = m[0] * m[5] * m[10] - 
			m[0] * m[6] * m[9] - 
			m[4] * m[1] * m[10] + 
			m[4] * m[2] * m[9] + 
			m[8] * m[1] * m[6] - 
			m[8] * m[2] * m[5];
		
		det = m[0] * inv.m[0] + m[1] * inv.m[4] + m[2] * inv.m[8] + m[3] * inv.m[12];
		
		if (det == 0)
			return This();
		
		det = 1.0 / det;
		
		foreach( i; 0 .. 16 )
			inv.m[i] = inv.m[i] * det;
		
		return inv;
	}
	
public:
	This translate( T x, T y, T z = 0 ) {
		return this.apply(
			This( [
					1, 0, 0, 0,
					0, 1, 0, 0,
					0, 0, 1, 0,
					x, y, z, 1
				] )
			);
	}
	This translate( V2T vec ) {
		return translate( vec.x, vec.y );
	}
	
	This rotate( Angle angle ) {
		float asin = angle.sinOf, acos = angle.cosOf;
		
		return this.apply(
			This(
					acos,  -asin, 0,  0,
					asin,  acos,  0,  0,
					0,    0,    1,  0,
					0,    0,    0,  1
				)
			);
	}
	This rotate( V2T center, Angle angle ) {
		return this.translate( -center ).rotate( angle ).translate( center );
	}
	
	This scale( T u ) {
		return this.apply(
			This(
					u, 0, 0, 0,
					0, u, 0, 0,
					0, 0, u, 0,
					0, 0, 0, 1
				)
			);
	}
	This scale( T x, T y, T z = 1 ) {
		return this.apply(
			This(
					x, 0, 0, 0,
					0, y, 0, 0,
					0, 0, z, 0,
					0, 0, 0, 1
				)
			);
	}

public:
	static This ortho( V2T size ) {
		return This(
				2f / size.x, 0, 0, 0,
				0, -2f / size.y, 0, 0,
				0, 0, 1f / 32f, 0,
				-1, 1, 0, 1
			);
	}
	
public:
	This opBinary( string s : "*" )( This mat ) {
		return this.apply( mat );
	}
	
}
mixin template Matrix_mix_graph() {
	
public:
	ref This force() {
		glMatrix( this );
		return this;
	}
	
}