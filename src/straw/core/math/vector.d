﻿module straw.core.math.vector;

import std.conv;
import std.math;
import std.traits;
import std.typetuple;
import straw.core.math.angle;
import straw.core.object;
import straw.core.utils.typetuple;

// TODO unittests

final struct Vec( T_, ubyte S_, string cookie = "" )
	if( isNumeric!T_ && S_ > 0 )
{
	mixin( StrawObject );

public:
	alias T = T_;
	enum S = S_;

	alias length = S;
	
public:
	T[ S ] array = 0;
	
public:
	/// Universal one-value constructor
	this( T2 )( T2 unit )
		if( isNumeric!T2 )
	{
		T u = cast( T ) unit;

		foreach( a; 0 .. S )
			array[ a ] = u;
	}

	/// Universal multi-value constructor
	this( Args ... )( Args args )	{
		mixin({
				string result;
				size_t sI;

				foreach( argI, Arg; Args ) {
					static if( isNumeric!Arg ) {
						result ~= "array[ " ~ sI.to!string ~ " ] = cast(T) args[ " ~ argI.to!string ~ " ];";
						sI += 1;
					}
					else static if( is( Arg : Vec!( T2, S2, cookie2 ), T2, ubyte S2, string cookie2 ) ) {
						foreach( aargI; 0 .. S2 )
							result ~= "array[ " ~ (sI + aargI).to!string ~ " ] = cast(T) args[ " ~ argI.to!string ~ " ][ " ~ aargI.to!string ~ " ];";

						sI += S2;
					}
					else static assert( "Unsupported vector constructor value type: " ~ Arg.stringof );

					assert( sI <= S, "Arguments dimensions exceed vector dimensions" );
				}

				assert( sI == S );

				return result;
			}());
	}

	this( string var ) {
		fromString( var );
	}
	
public:
	@property float size() const {
		double res = 0;
		foreach( a; Iota!S )
			res += cast( float ) array[ a ] * cast( float ) array[ a ];
		
		return sqrt( res );
	}
	
	@property This normalized() const {
		return this / cast( T ) size;
	}
	
public:
	static if( S == 2 )
	@property This normalOf() const {
		return This( -this.y, this.x ).normalized;
	}
	
	T dotProduct( This other ) const {
		T result = 0;
		
		foreach( a; 0 .. S )
			result += array[ a ] * other.array[ a ]; 
		
		return result;
	}
	
	static if( S == 2 )
	This rotated( This center, Angle angle ) const {
		This tmpVec = this - center;
		return center + This( tmpVec.x * angle.cos + tmpVec.y * angle.sin, tmpVec.y * angle.cos - tmpVec.x * angle.sin );
	}

	static if( S == 2 )
	This rotated( Angle angle ) const {
		return This( this.x * angle.cos + this.y * angle.sin, this.y * angle.cos - this.x * angle.sin );
	}

public:
	@property This abs() {
		This ret;
		foreach( a; 0 .. S )
			ret.array[ a ] = array[ a ].abs;

		return ret;
	}

public:
	void assign( T unit ) {
		foreach( a; 0 .. S )
			array[ a ] = unit;
	}
	void assign( T[ S ] vals ... ) {
		foreach( a; 0 .. S )
			array[ a ] = cast( T ) vals[ a ];
	}
	
public:
	/// Mixin functions like oneMore, oneLes, oneMoreOrEqual, ...
	mixin({
			enum superVariants = [
				[ "every", "!", "false" ],
				[ "one", "", "true" ]
			];
			enum variants = [
				[ "Less", "<" ],
				[ "LessOrEqual", "<=" ],
				[ "More", ">" ],
				[ "MoreOrEqual", ">=" ]
			];

			string result;

			foreach( superVariant; superVariants ) {
				foreach( variant; variants ) {

					result ~= "
						bool " ~ superVariant[0] ~ variant[0] ~ "( T2, ubyte S2, string cookie2 )( Vec!( T2, S2, cookie2 ) vec ) const
							if( isNumeric!T2 && S2 <= S )
						{
							foreach( a; 0 .. S2 )
								if( " ~ superVariant[1] ~ "( array[ a ] " ~ variant[1] ~ " vec.array[ a ] ) ) return " ~ superVariant[2] ~ ";
							
							return !" ~ superVariant[2] ~ ";
						}

						bool " ~ superVariant[0] ~ variant[0] ~ "( T2 )( T2 val ) const
							if( isNumeric!T2 )
						{
							foreach( a; 0 .. S )
								if( " ~ superVariant[1] ~ "( array[ a ] " ~ variant[1] ~ " val ) ) return " ~ superVariant[2] ~ ";
							
							return !" ~ superVariant[2] ~ ";
						}
					";
				}
			}

			return result;
		}());

	bool between( T2, T3 )( Vec!( T2, S ) v, Vec!( T3, S ) v2 ) const {
		return everyMoreOrEqual( v ) && everyLess( v2 );
	}
	
public:
	string toString() const {
		string ret = array[ 0 ].to!string;
		foreach( a; 1 .. S )
			ret ~= "x" ~ array[ a ].to!string;
		
		return ret;
	}
	void fromString( string str ) {
		size_t pos = 0, lastpos = 0, ind = 0, len = str.length;
		while( pos < len ) {
			
			if( str[ pos ] == 'x' ) {
				if( pos > lastpos ) {
					array[ ind ] = str[ lastpos .. pos ].to!T;
					if( ind == S - 1 ) return;
				}
				
				ind ++;
				lastpos = pos + 1;
			}
			
			pos ++;
		}
		
		if( pos > lastpos )
			array[ ind++ ] = str[ lastpos .. pos ].to!T;
	}

	Vec!( T2, S2, cookie2 ) castTo( T2, ubyte S2 = S, string cookie2 = cookie )() const
		if( S2 <= S && isNumeric!T2 )
	{
		static if( is( T2 == T ) )
			static assert( 0, "Converting to the same vector type" );
		
		Vec!( T2, S2, cookie2 ) ret;
		
		foreach( a; 0 .. S2 )
			ret.array[ a ] = cast( T2 ) array[ a ];
		
		return ret;
	}
	auto castTo( V : Vec!( T2, S2, cookie2 ), T2, ubyte S2, string cookie2 )() const
		if( S2 <= S )
	{
		return this.castTo!( T2, S2, cookie2 );
	}
	
public:
	void opAssign( string str ) {
		fromString( str );
	}
	
public:
	// - unary
	This opUnary( string op : "-" )() const {
		Vec!( T, S ) ret = this;
		foreach( a; 0 .. S )
			ret.array[ a ] *= -1;
		
		return ret;
	}
	
	// + - VEC
	This opBinary( string op : "+" )( const This v ) const {
		This ret = this;
		foreach( a; 0 .. S )
			ret.array[ a ] += v.array[ a ];
		
		return ret;
	}
	This opBinary( string op : "-" )( const This v ) const {
		This ret = this;
		foreach( a; 0 .. S )
			ret.array[ a ] -= v.array[ a ];
		
		return ret;
	}
	This opBinary( string op : "*" )( const This v ) const {
		This ret = this;
		foreach( a; 0 .. S )
			ret.array[ a ] *= v.array[ a ];
		
		return ret;
	}
	This opBinary( string op : "/" )( const This v ) const {
		This ret = this;
		foreach( a; 0 .. S )
			ret.array[ a ] /= v.array[ a ];
		
		return ret;
	}
	This opBinary( string op : "%" )( const This v ) const {
		This ret = this;
		foreach( a; 0 .. S )
			ret.array[ a ] %= v.array[ a ];
		
		return ret;
	}
	
	// + - * / UNIT
	This opBinary( string op : "+" )( T u ) const {
		This ret = this;
		foreach( a; 0 .. S )
			ret.array[ a ] += u;
		
		return ret;
	}
	This opBinary( string op : "-" )( T u ) const {
		This ret = this;
		foreach( a; 0 .. S )
			ret.array[ a ] -= u;
		
		return ret;
	}
	This opBinary( string op : "*" )( T u ) const {
		This ret = this;
		foreach( a; 0 .. S )
			ret.array[ a ] *= u;
		
		return ret;
	}
	This opBinary( string op : "/" )( T u ) const {
		This ret = this;
		foreach( a; 0 .. S )
			ret.array[ a ] /= u;
		
		return ret;
	}
	This opBinary( string op : "%" )( T u ) const {
		This ret = this;
		foreach( a; 0 .. S )
			ret.array[ a ] %= u;
		
		return ret;
	}
	
	// += -= VEC
	void opOpAssign( string op : "+" )( const This v ) {
		foreach( a; 0 .. S )
			array[ a ] += v.array[ a ];
	}
	void opOpAssign( string op : "-" )( const This v ) {
		foreach( a; 0 .. S )
			array[ a ] -= v.array[ a ];
	}
	void opOpAssign( string op : "*" )( const This v ) {
		foreach( a; 0 .. S )
			array[ a ] *= v.array[ a ];
	}
	void opOpAssign( string op : "/" )( const This v ) {
		foreach( a; 0 .. S )
			array[ a ] /= v.array[ a ];
	}
	void opOpAssign( string op : "%" )( const This v ) {
		foreach( a; 0 .. S )
			array[ a ] %= v.array[ a ];
	}

	// += -= * / UNIT
	void opOpAssign( string op : "+" )( T u ) {
		foreach( a; 0 .. S )
			array[ a ] += u;
	}
	void opOpAssign( string op : "-" )( T u ) {
		foreach( a; 0 .. S )
			array[ a ] -= u;
	}
	void opOpAssign( string op : "*" )( T u ) {
		foreach( a; 0 .. S )
			array[ a ] *= u;
	}
	void opOpAssign( string op : "/" )( T u ) {
		foreach( a; 0 .. S )
			array[ a ] /= u;
	}
	void opOpAssign( string op : "%" )( T u ) {
		foreach( a; 0 .. S )
			array[ a ] %= u;
	}

	bool opEquals( const This v ) const {
		foreach( a; 0 .. S ) {
			if( array[ a ] != v.array[ a ] )
				return false;
		}

		return true;
	}

public:
	This floored( real unit = 1 ) const {
		This tmp;
		
		foreach( a; 0 .. S ) {
			tmp.array[ a ] = cast( T )( std.math.floor( cast( real )( array[ a ] ) / unit ) * unit );
		}
		
		return tmp;
	}
	This rounded( real unit = 1 ) const {
		This tmp;
		
		foreach( a; 0 .. S ) {
			tmp.array[ a ] = cast( T )( std.math.round( cast( real )( array[ a ] ) / unit ) * unit );
		}
		
		return tmp;
	}
	
public:
	This limitedTo( This lower, This upper ) const {
		This result = this;
		foreach( a; 0 .. S ) {
			if( result.array[ a ] < lower.array[ a ] )
				result.array[ a ] = lower.array[ a ];
			
			else if( result.array[ a ] > upper.array[ a ] )
				result.array[ a ] = upper.array[ a ];
		}
		return result;
	}
	
public:
	enum dispatchLetterSets = {
		string[] result;
		
		result ~= "xyz";
		
		if( S == 2 ) {
			result ~= "wh";
			result ~= "lt";
			
		} else if( S == 4 ) {
			result ~= "  wh";
			result ~= "ltrb";
		}
		
		return result;
	}();
	private static bool opDispatchValid( string str, bool denyZero = false ) {
		if( str.length == 0 )
			return false;
		
		foreach( ch; str ) {
			if( ch == '_' )
				continue;
			
			bool isOkay = false;
			foreach( set; dispatchLetterSets ) {
				foreach( sch; set ) {
					if( sch == ch ) {
						isOkay = true;
						break;
					}
				}
				
				if( isOkay )
					break;
			}
			
			if( !isOkay )
				return false;
		}
		
		return true;
	}
	private static string opDispatchStr( char ch ) {
		if( ch == '_' )
			return "0";
		
		foreach( set; dispatchLetterSets ) {
			foreach( a, sch; set ) {
				if( sch == ch )
					return "array[ " ~ a.to!string ~ "]";
			}
		}
		
		assert( 0, "The dispatch letter '" ~ ch ~ "' is invalid." );
	}
	
	// getters
	T opDispatch( string str )() const
		if( opDispatchValid( str ) && str.length == 1 )
	{
		mixin( "return " ~ opDispatchStr( str[ 0 ] ) ~ ";" );
	}	
	ref T opDispatch( string str )()
		if( opDispatchValid( str ) && str.length == 1 )
	{
		mixin( "return " ~ opDispatchStr( str[ 0 ] ) ~ ";" );
	}

	Vec!( T, str.length ) opDispatch( string str )() const
		if( opDispatchValid( str ) && str.length > 1 )
	{
		template mix( string s, ubyte index = 0 ) {
			static if( s.length - index <= 0 )
				enum mix = "";
			else {
				enum mix = "ret.array[ " ~ index.to!string ~ "] = " ~ opDispatchStr( s[ index ] ) ~ "; \n" ~ mix!( s, index + 1 );
			}
		}
		
		Vec!( T, str.length ) ret;
		mixin( mix!str );
		return ret;
	}
	
	// setter - T2
	void opDispatch( string str, T2 )( T2 unit )
		if( opDispatchValid( str, false ) && str.length > 1 )
	{
		this = This( unit );
	}
	void opDispatch( string str, T2 )( Vec!( T2, str.length ) v )
		if( opDispatchValid( str, false ) )
	{
		this = This( v );
	}
	
public:
	T opIndex( int index ) const {
		assert( index < S && index >= 0 );
		return array[ index ];
	}
	ref T opIndex( int index ) {
		assert( index < S && index >= 0 );
		return array[ index ];
	}

public:
	static auto range()( This first, This second = This( 0 ) ) {
		return VecRange!( T_, S_, cookie )( first, second );
	}

public:
	size_t toHash() const nothrow @safe {
		// TODO better :D
		
		size_t result;
		foreach( a; 0 .. S )
			result ^= cast( size_t ) array[ a ];
		
		return result;
	}

}

struct VecRange( T, ubyte S, string cookie )
	if( S > 0 )
{
	
private:
	alias V = Vec!( T, S, cookie );
	V lower, upper;
	
public:
	this( V first, V second = V( 0 ) ) {
		foreach( a; 0 .. S ) {
			if( first[ a ] < second[ a ] ) {
				lower[ a ] = first[ a ];
				upper[ a ] = second[ a ];
				
			} else {
				lower[ a ] = second[ a ];
				upper[ a ] = first[ a ];
			}
		}
	}
	this( T[ S * 2 ] arr ... ) {
		foreach( a; 0 .. S ) {
			if( arr[ a ] < arr[ S + a ] ) {
				lower[ a ] = arr[ a ];
				upper[ a ] = arr[ S + a ];
				
			} else {
				lower[ a ] = arr[ S + a ];
				upper[ a ] = arr[ a ];
			}
		}
	}
	
public:
	private int opApplyHelper( ubyte depth )( int delegate( V ptr ) dg, ref V iterator ) {
		int ret;
		
		for( iterator[ depth ] = lower[ depth ]; iterator[ depth ] < upper[ depth ]; iterator[ depth ] ++ ) {
			
			static if( depth == S - 1 )
				ret = dg( iterator );
			else
				ret = opApplyHelper!( depth + 1 )( dg, iterator );
			
			if( ret )
				return ret;
		}
		
		return 0;
	}

	int opApply( int delegate( V ) dg ) {
		auto it = lower;
		return opApplyHelper!0( dg, it );
	}

}

alias Vec2( T, string cookie = "" ) = Vec!( T, 2, cookie );
alias Vec3( T, string cookie = "" ) = Vec!( T, 3, cookie );
alias Vec4( T, string cookie = "" ) = Vec!( T, 4, cookie );

alias V2I = Vec2!int;
alias V2F = Vec2!float;

alias V3F = Vec3!float;