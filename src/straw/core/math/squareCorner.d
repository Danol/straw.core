﻿module straw.core.math.squareCorner;

import std.typecons;

struct SquareCorner {

public:
	enum : ubyte {
		leftTop = 0,
		rightTop = 1,
		leftBottom = 2,
		rightBottom = 3
	}

private:
	ubyte value_;

public:
	this( ubyte value ) {
		value_ = value;
	}

public:
	alias value this;
	@property int value() const {
		return cast(ubyte) value_;
	}

	@property bool left() const {
		return ( value_ & 1 ) == 0;
	}
	@property void left( bool set ) {
		if( set )
			value_ &= ~1;
		else
			value_ |= 1;
	}

	@property bool right() const {
		return ( value_ & 1 ) != 0;
	}
	@property void right( bool set ) {
		if( set )
			value_ |= 1;
		else
			value_ &= ~1;
	}

	@property bool top() const {
		return ( value_ & 2 ) == 0;
	}
	@property void top( bool set ) {
		if( set )
			value_ &= ~2;
		else
			value_ |= 2;
	}
	
	@property bool bottom() const {
		return ( value_ & 2 ) != 0;
	}
	@property void bottom( bool set ) {
		if( set )
			value_ |= 2;
		else
			value_ &= ~2;
	}


}