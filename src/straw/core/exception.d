﻿module straw.core.exception;

import std.conv;
import std.range;
import std.traits;
import straw.core.object;
import straw.core.utils.typetuple;

public {
	import std.exception;
}

final class NotImplementedException : Exception {

public:
	this() {
		super( "Not implemented" );
	}

}

class StrawException : Throwable {
	mixin( StrawObjectLite );

private:
	string identifier_;
	string[ string ] keysAndValues;

public:
	final @property string identifier() {
		return identifier_;
	}

public:
	this( Args ... )( string identifier, auto ref Args args ) {
		identifier_ = identifier;
	
		addArguments( args );

		super( toString );
	}

public:
	final This addArguments( Args ... )( auto ref Args args ) {
		void helperFunc( size_t i = 0 )() {
			static if( i >= Args.length ) {
				// Do nothing

			} else static if( is( Args[ i ] == string ) ) {
				static assert( i != Args.length - 1, "Missing parameter value (last argument)" );

				keysAndValues[ args[ i ] ] = args[ i + 1 ].to!string;
				helperFunc!( i + 2 )();
				
			} else static if( is( ReturnType!( args[ i ].__exceptionInfo ) == string[ string ] ) ) {
				foreach( key, value; args[ i ].__exceptionInfo )
					keysAndValues[ key ] = value;

				helperFunc!( i + 1 )();
				
			} else
				static assert( 0, "Argument is not parseable [" ~ i.to!string ~ "] | " ~ Args.stringof );
		}

		helperFunc();

		return this;
	}
	final This changeIdentifier( string newIdentifier ) {
		identifier_ = newIdentifier;

		return this;
	}

public:
	final override string toString() {
		string result = "\nException: '" ~ identifier ~ "'";
		foreach( key, value; keysAndValues )
			result ~= "\n\t" ~ key ~ ": '" ~ value ~ "'";

		return result;
	}

}

class DerivedStrawException( string cookie, RequiredArgs ... ) : StrawException {

public:
	this( Args ... )( string identifier, auto ref Args args )	{
		static assert( Args.length >= RequiredArgs.length );

		foreach( i; Iota!( RequiredArgs.length ) )
			static assert( is( Args[ i ] : RequiredArgs[ i ] ) );

		super( cookie ~ "." ~ identifier, args );
	}

}