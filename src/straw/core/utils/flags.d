﻿module straw.core.utils.flags;

import core.bitop;
import straw.core.object.object;

struct Flags( string cookie ) {
	mixin( StrawObjectLite );

private:
	alias Data = uint;
	Data data;

public:
	/// Returns a Flags with bit #bitNum set
	static This bit( ubyte bitNum )() {
		return Flags( 1 << bitNum );
	}

private:
	this( Data data ) {
		this.data = data;
	}

public:
	bool hasCommonBits( in This other ) const {
		return ( data & other.data ) != 0;
	}

	@property bool empty() const {
		return data == 0;
	}
	@property ubyte bitCount() const {
		return cast( ubyte ) data.popcnt;
	}

	/// Returns array of offsets of the bits set
	immutable( ubyte[] ) bitsArray() const {
		Data dt = data;
		immutable( ubyte )[] result;

		while( dt ) {
			ubyte bit = cast( ubyte ) bsf( dt );
			result ~= bit;
			dt = dt & ~( 1 << bit );
		}

		return result;
	}

	/// Returns first set bit; undefined result when no bits are set (in debug, it throws an exception)
	ubyte firstBit() const {
		assert( data != 0, "no bits are set" );

		return cast( ubyte ) bsf( data );
	}

public:
	void opOpAssign( string op : "+" )( in This other ) {
		data |= other.data;
	}
	void opOpAssign( string op : "-" )( in This other ) {
		data &= ~other.data;
	}

public:
	/// Returns if all flags in other are in this as well
	bool opBinaryRight( string op : "in" )( in This other ) {
		return ( other.data & data ) == other.data;
	}

	This opBinary( string op : "+" )( in This other ) {
		return This( data | other.data );
	}
	This opBinary( string op : "-" )( in This other ) {
		return This( data & ~other.data );
	}
	This opBinary( string op : "&" )( in This other ) {
		return This( data & other.data );
	}

}