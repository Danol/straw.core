﻿module straw.core.utils.typetuple;

import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.traits;
import std.typetuple;

final abstract class EnclosedTypeTuple( Args ... ) {
	alias tuple = Args;
}

template Tuplify( alias array_ )
	if( isArray!( typeof(array_) ) )
{
	enum Tuplify = mixin( "TypeTuple!( " ~ iota( array_.length ).map!( a => "array_[ " ~ a.to!string ~ " ]" ).joiner( ", " ).array ~ " )" );
}

template Iota( ulong num ) {
	mixin( "alias Iota = TypeTuple!( " ~ iota( num ).map!( x => x.to!string ).joiner( ", " ).array ~ " );" );
}