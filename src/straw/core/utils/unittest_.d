﻿module straw.core.utils.unittest_;
version( unittest ):

import std.conv;
import std.stdio;

// TODO Bettah dude

private struct UnittestFrame {
	string file;
	size_t line;
	string func;
}

private UnittestFrame[] unittestFrame;

void unittest_start( string file = __FILE__, size_t line = __LINE__, string func = __FUNCTION__ )() {
	unittestFrame ~= UnittestFrame( file, line, func );
	writeln( ".. Unittest in file '", line, "#", file, "' running..." );
}

void unittest_end( string file = __FILE__, size_t line = __LINE__, string func = __FUNCTION__ )() {
	assert( unittestFrame.length, "Calling unittest_end without calling unittest_start before." );

	auto fr = unittestFrame[ $ - 1 ];
	unittestFrame.length --;

	assert( fr.file == file && fr.func == func, "You did not call unittest_end in after calling unittest_start on '" ~ fr.line.to!string ~ "#" ~ fr.file ~ "'" );

	writeln( ">> Unittest in file '", fr.line, "#", file, "' passed" );
}