﻿module straw.core.hooks; 

import core.sync.rwmutex;
import std.algorithm;
import std.array;
import std.conv;
import std.typetuple;
import std.stdio;

//version = debugHooks;

/// Registers a function #func to the _moduleInitList. All functions registered are then called using _moduleInit__call (recommended to put into your main function).
mixin template _moduleInit( alias func )
	if( is( typeof( func ) == void function() ) )
{
	private alias __moduleInit = __moduleInitRegisterer!func;
}

private __gshared void function()[] _moduleInitList;
template __moduleInitRegisterer( alias func ) {
	shared static this() {
		_moduleInitList ~= func;
	}
}

version( debugHooks )
	private static __gshared size_t hookCallLevel = 0;

/// Calls all the function registered via _moduleInit.
void _moduleInit__call() {
	foreach( f; _moduleInitList ) f();
}

template hook( string cookie, Args ... ) {
	
public:
	alias Function = void function( auto ref Args );

private:
	final class Record {
		
	public:
		alias This = typeof( this );
		
	private:
		string name;
		bool sortToggle_;
		Function func;
		Record[] callTheseBefore;
		
	public:
		this( string name ) {
			this.name = name;
			this.sortToggle_ = sortToggle;
		}
		
	public:
		This callAfter( string[] names ... ) {
			foreach( nm; names ) {
				foreach( rec; callTheseBefore ) {
					if( rec.name == nm )
						break;
				}

				Record rec = addOrFind( nm );

				synchronized( mutex.writer ) {
					callTheseBefore ~= rec;
					sorted = false;
				}
			}
			
			return this;
		}
		This callBefore( string[] names ... ) {
			foreach( nm; names ) {
				Record rec = addOrFind( nm );
				foreach( rrec; rec.callTheseBefore ) {
					if( rrec.name == nm )
						break;
				}
				
				synchronized( mutex.writer ) {
					rec.callTheseBefore ~= this;
					sorted = false;
				}
			}
			
			return this;
		}
		
	public:
		This opAssign( Function func ) {
			assert( this.func is null, "Hook record function already assigned! (" ~ cookie ~ "." ~ name ~ ")" );
			this.func = func;
			return this;
		}
		
	}
	
private:
	__gshared {
		bool sorted, sortToggle;
		size_t anonymousCnt;
		
		ReadWriteMutex mutex;
		
		Record[ string ] unsortedTable;
		Record[] sortedList;
	}

private:
	shared static this() {
		mutex = new ReadWriteMutex;
	}
	shared static ~this() {
		delete mutex;
	}

public:
	Record add( string identifier = "" ) {
		auto res = addOrFind( identifier );
		assert( res.func is null, "Hook record '" ~ cookie ~ "." ~ identifier ~ "' already added!" );
		
		return res;
	}
	void call( auto ref Args args ) {
		mutex.reader.lock();
		
		if( !sorted ) {
			mutex.reader.unlock();
			sort(); // Sort locks writer
			mutex.reader.lock();
		}

		scope( exit ) mutex.reader.unlock();

		version( debugHooks ) {
			hookCallLevel ++;
			writeln( "   ".replicate( hookCallLevel - 1 ), "Hook '", cookie, "' called" );
		}

		foreach( rec; sortedList ) {
			version( debugHooks )
				writeln( "   ".replicate( hookCallLevel - 1 ), " >> '", rec.name, "'" );

			if( rec.func )
				rec.func();
		}

		version( debugHooks ) {
			hookCallLevel --;
		}
	}
	
protected:
	Record addOrFind( string identifier ) {
		if( !identifier.length )
			identifier = "#" ~ ( ++anonymousCnt ).to!string;

		synchronized( mutex.writer ) {
			if( auto e = identifier in unsortedTable )
				return *e;
			
			else {
				auto rec = new Record( identifier );
				unsortedTable[ identifier ] = rec;
				sortedList ~= rec;

				sorted = false;
				return rec;
			}
		}
	}
	
	void sort() {
		synchronized( mutex.writer ) {
			if( sorted )
				return;
			
			// sortedList is divided into sorted part [ 0 .. sortedCnt ] and unsorted part [ sortedCnt .. $ ]
			size_t sortedCnt, prevSortedCnt;
			size_t sortedListLength = sortedList.length;
			
			while( sortedCnt < sortedListLength ) {
				// List through the unsorted part
				foreach( size_t i; sortedCnt .. sortedListLength ) {
					auto rec = sortedList[ i ];
					
					// If all callTheseBefore are met
					if(	rec.callTheseBefore.all!(
							( req ) => sortedList[ 0 .. sortedCnt ].any!(
								curr => curr is req
								)
							)
						)
					{
						auto tmp = sortedList[ sortedCnt ];
						sortedList[ sortedCnt ] = rec;
						sortedList[ i ] = tmp;
						
						sortedCnt ++;
						i --;
					}
				}
				
				if( sortedCnt == prevSortedCnt ) {
					// TODO error
					assert( 0, "There is some recursion in hooks '" ~ cookie ~ "', participating: " );
				}
				
				prevSortedCnt = sortedCnt;
			}

			sorted =  true;
		}
	}
	
}

/**
 * Alias for a singleton instance of type #Type that is created on hook #Hook
 * */
template hookedSingleton( Type, alias CtorHook : hook!ctorCookie, alias DtorHook : hook!dtorCookie, string ctorCookie, string dtorCookie )
	if( is( Type == class ) )
{
	static __gshared Type hookedSingleton;

	mixin _moduleInit!({
			CtorHook.add() = {
				hookedSingleton = new Type();
			};
			DtorHook.add() = {
				delete hookedSingleton;
			};
		});
}