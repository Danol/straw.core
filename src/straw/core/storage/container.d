﻿module straw.core.storage.container;

import core.stdc.stdlib;
import std.algorithm;
import std.array;
import core.stdc.string;
import std.math;
import std.traits;
import straw.core.utils.unittest_;
import straw.core.utils.typetuple;

/**
 * !! DOES NOT PRESERVE ORDER (when removing) !!
 * !! IS NOT MEMORY SAFE (doesn't call destructors n' stuff) !!
 * .
 * Implements a Last-in-first-out container based on standard custom malloc-handled stuff.
 * The removing policy is swapping - the last item in the array is put to the place of the removed element.
 * */
struct Container( T, bool indirectionsOverride = false ) {
	static assert( !hasIndirections!T || indirectionsOverride, "Straw container does not collaborate with GC, so it doesn't support indirections (type " ~ T.stringof ~ ")" );

private:
	static struct Range {

	private:
		T* ptr, endPtr;

	public:
		bool empty() {
			return ptr is endPtr;
		}
		ref T front() {
			return *ptr;
		}
		void popFront() {
			ptr ++;
		}

	}
	static struct Slice {
		size_t start, end;
	}

public:
	alias This = typeof( this );

private:
	T* ptr_;
	size_t length_, capacity_ = 0;

public:
	@disable this(this);
	~this() {
		free( ptr_ );
	}

public:
	/// Returns the count of the items in the container
	@property size_t length() const {
		return length_;
	}
	/// Returns how many items the current allocation has capacity to carry.
	@property size_t capacity() const {
		return capacity_;
	}

	@property const(T)* ptr() const {
		return ptr_;
	}

public:
	@property ref T front() {
		assert( length );

		return ptr_[ 0 ]; 
	}
	@property const(T) front() const {
		assert( length );
		
		return ptr_[ 0 ]; 
	}

	@property ref T back() {
		assert( length );
		
		return ptr_[ length - 1 ]; 
	}
	@property const(T) back() const {
		assert( length );
		
		return ptr_[ length - 1 ]; 
	}

public:
	/// Make sure at least cnt pushes will be without allocatiob
	void reserve( in size_t cnt ) {
		size_t targetCnt = length + cnt;

		if( capacity >= targetCnt )
			return;

		if( capacity == 0 )
			capacity_ = 1;

		while( capacity < targetCnt )
			capacity_ *= 2;

		ptr_ = cast(T*) realloc( ptr_, capacity * T.sizeof );
	}

	/// Pushes item to the back and returns it's position in the array.
	/// !!! The position might change in the future because of the swapping policy !!!
	void pushBack( Args ... )( Args args )
		if( Args.length > 0 )
	{
		reserve( Args.length );

		foreach( i; Iota!( Args.length ) ) {
			static assert( is( Args[ i ] : T ), "Every item of pushBack must be of type T (or implicitly convertible)" );
			ptr_[ length + i ] = args[ i ];
		}

		length_ += Args.length;
	}
	void pushBackNTimes( size_t times, Args ... )( Args args )
		if( Args.length > 0 )
	{
		reserve( Args.length * times );

		foreach( a; Iota!( times ) ) {
			foreach( b; Iota!( Args.length ) ) {
				static assert( is( Args[ b ] == T ), "Every item of pushBack must be of type T (or implicitly convertible)" );
				ptr_[ length + a * Args.length + b ] = args[ b ];
			}
		}
		
		length_ += times * Args.length;
	}
	/// Pushes back an array of items.
	void pushBackArray( T[] list ) {
		size_t listLength = list.length;
		reserve( listLength );
		memcpy( ptr_ + length, list.ptr, T.sizeof * listLength );
		length_ += listLength;
	}
	void pushBackContainer( ref This other ) {
		reserve( other.length );
		memcpy( ptr_ + length, other.ptr, other.length * T.sizeof );
		length_ += other.length;
	}

public:
	/// Pops out the last item in the container
	void popBack() {
		assert( length );
		
		length_ --;
	}
	/// Pops out an item on the pos index.
	/// Takes the last item in the container and puts in on the removed item's place.
	void popIndex( size_t index ) {
		assert( index < length );

		if( index != length_ - 1 )
			ptr_[ index ] = ptr_[ length - 1 ];
		
		length_ --;
	}
	/// Pops first ocurrence of item item (uses find).
	///	Returns the index it found the occurence on (if not found, returns -1).
	/// Takes the last item in the container and puts in on the removed item's place,
	size_t popFirstFound()( T item ) {
		size_t i = find( item );
		
		if( i == -1 )
			return -1;
		
		popIndex( i );
		return i;
	}

	void clear() {
		length_ = 0;
	}
	/// Unallocates the array
	void clearProperly() {
		length_ = 0;
		free( ptr_ );
		ptr_ = null;
	}

public:
	/// Tries to find the item in the container.
	/// If found, returns the index of the item (first occurence). If not found, returns -1.
	/// If the T has indirections (std.traits.hasIndirections), uses is, otherwise uses ==
	size_t find()( T item ) const	{
		foreach( i; 0 .. length ) {
			static if( hasIndirections!T ) {
				if( ptr_[ i ] is item )
					return ptr_[ i ];
			}
			else {
				if( ptr_[ i ] == item )
					return ptr_[ i ];
			}
		}
		
		return -1;
	}
	
public:
	Slice opSlice( size_t i )( size_t start, size_t end ) const {
		return Slice( start, end );
	}

	Range opIndex() {
		return Range( ptr_, ptr_ + length );
	}
	Range opIndex( in Slice slice ) {
		return Range( ptr_ + slice.start, ptr_ + slice.end );
	}
	ref T opIndex( size_t index ) {
		assert( index < length );

		return ptr_[ index ];
	}
	ref const( T ) opIndex( size_t index ) const {
		assert( index < length );

		return ptr_[ index ];
	}
	
	size_t opDollar( size_t pos )() const {
		return length;
	}

	void opOpAssign( string operator : "~" )( T item ) {
		pushBack( item );
	}
	void opOpAssign( string operator : "~" )( T[] array ) {
		pushBackArray( array );
	}

public:
	int opApply( int delegate( ref T ) dg ) {
		foreach( i; 0 .. length ) {
			if( int r = dg( ptr_[i] ) )
				return r;
		}

		return 0;
	}
	int opApply( int delegate( size_t, ref T ) dg ) {
		foreach( i; 0 .. length ) {
			if( int r = dg( i, ptr_[i] ) )
				return r;
		}
		
		return 0;
	}

public:
	/// Assigning clears the source, moving all the contents to the target (really, really fast)
	ref This opAssign( ref This other ) {
		this.capacity_ = other.capacity;
		this.length_ = other.length;
		this.ptr_ = other.ptr_;

		other.capacity_ = 0;
		other.length_ = 0;
		other.ptr_ = null;

		return this;
	}

}