﻿module straw.core.storage.hashTable;

alias HashTable( Key, Value ) = Value[ Key ];