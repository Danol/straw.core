﻿module straw.core.object.traits;

import std.array;
import std.conv;
import std.typetuple;
import straw.core.object;

enum StrawObject_MemberType {
	staticFunction,
	dynamicFunction,

	staticField,
	dynamicField,

	enumField
}

/**
 * Returns TypeTuple with info of all the T's member (T must have StrawObject mixed in).
 * .
 * Info classes have following fields:
 * 	type (StrawObject_MembeType): says if the member is static/dynamic function/field/enum
 * 	name (string): member name
 * 	T (type): type of the member
 * 	attributes (TypeTuple): typetuple of all member's attributes
 * .
 * Additionally, functions have these fields:
 *  overloadId (size_t): overload index the member is accecsible from via __traits( getOverloads, T, memName )[ overloadIndex ]
 *  .
 * 	auto call( [auto ref T instance,] args ) : yaay, you can call a function like this, even the private one! :)
 * .
 * Additionally, fields have these fields:
 * 	auto getRef( [auto ref T instance] ) : returns the reference of the field (even if the field is private)
 * */
template MembersInfo( T )
	if( isStrawObject!T )
{
	alias MembersInfo = T.STRAW_OBJECT_MEMBER_LIST!();
}

/**
 * Matches attributes that have member memName (checked via __traits( hasMember, ... ) )
 * */
template MIAF_HasMember( string memName ) {
	bool MIAF_HasMember( MemberInfo, alias Attribute )() {
		return __traits( hasMember, Attribute, memName );
	}
}

/**
 * Returns TypeTuple of member-attribute sets of type T which match specified Filter.
 * If the member has multiple attributes which match the filter, allowMultipleMatcherPerMember decides if the lookup will stop with the first attribute matched or the member will end up multiple times in the result TypeTuple.
 * For filters, use MIAF_ prefixed templates (or study them and make your own)
 * 
 * The returned items have these fields:
 * 	MemberInfo : matched member info, as from MemberInfo trait
 * 	Attribute : matched attribute
 * */
template MembersInfo_AttributeFilter( T, alias Filter, bool allowMultipleMatchesPerMember = true )
	if( isStrawObject!T )
{
	mixin( {
			string[] result;

			foreach( mi, memInfo; MembersInfo!T ) {
				foreach( ai, attr; memInfo.attributes ) {
					static if( Filter!( memInfo, attr ) ) {
						result ~= "MemberInfo_AttributeFilter_Result!( MembersInfo!T[ " ~ mi.to!string ~ " ], " ~ ai.to!string ~ " )";

						static if( !allowMultipleMatchesPerMember )
							break;
					}
				}
			}

			return "alias MembersInfo_AttributeFilter = TypeTuple!( " ~ result.join( ", " ) ~ " );";
		} () );
}

private final abstract class MemberInfo_AttributeFilter_Result( MemberInfo_, size_t attributeIndex ) {
	
public:
	alias MemberInfo = MemberInfo_;
	alias Attribute = MemberInfo_.attributes[ attributeIndex ];
	
}

mixin template StrawObject_traits() {

public:
	template STRAW_OBJECT_MEMBER_LIST() {
		alias STRAW_OBJECT_MEMBER_LIST = __STRAW_OBJECT_MEMBER_LIST!( __traits( allMembers, This ) );
	}

private:
	import std.traits;
	import std.typetuple;
	import straw.core.object.traits;

private:
	template __STRAW_OBJECT_MEMBER_LIST( membersToParse... ) {
		static if( membersToParse.length ) {
			enum currentMemberName = membersToParse[ 0 ];

			static if( currentMemberName == "" ) 
				alias CurrentMemberInfo = TypeTuple!();

			else static if( isSomeFunction!( __traits( getMember, This, currentMemberName ) ) )
				alias CurrentMemberInfo = __STRAW_OBJECT_MEMBER_FUNCTIONS_DATA!( currentMemberName );

			else static if( __traits( compiles, typeof( __traits( getMember, This, currentMemberName ) ) ) )
				alias CurrentMemberInfo = __STRAW_OBJECT_MEMBER_FIELD_DATA!( currentMemberName );


			else
				alias CurrentMemberInfo = TypeTuple!();

			alias __STRAW_OBJECT_MEMBER_LIST = TypeTuple!( CurrentMemberInfo, __STRAW_OBJECT_MEMBER_LIST!( membersToParse[ 1 .. $ ] ) );

		}
		else
			alias __STRAW_OBJECT_MEMBER_LIST = TypeTuple!();
	}

	template __STRAW_OBJECT_MEMBER_FUNCTIONS_DATA( string memName_, size_t overloadId = 0 ) {
		mixin( {
				import std.algorithm;
				import std.array;
				import std.conv;
				import std.range;

				return
					"alias __STRAW_OBJECT_MEMBER_FUNCTIONS_DATA = TypeTuple!("
						~ iota( 0, __traits( getOverloads, This, memName_ ).length ).map!( o => "__STRAW_OBJECT_MEMBER_FUNCTION_DATA!( memName_, " ~ o.to!string ~ " )" ).array.join( ", " )
						~ " );";
			} () );
	}
	final static abstract class __STRAW_OBJECT_MEMBER_FUNCTION_DATA( string memName_, size_t overloadId_ ) {

	public:
		enum type = __traits( isStaticFunction, __traits( getOverloads, This, memName_ )[ overloadId_ ] ) ? StrawObject_MemberType.staticFunction : StrawObject_MemberType.dynamicFunction;
		enum name = memName_;
		enum size_t overloadId = overloadId_;
		alias attributes = TypeTuple!( __traits( getAttributes, __traits( getOverloads, This, memName_ )[ overloadId_ ] ) );

	public:
		alias T = typeof( __traits( getOverloads, This, memName_ )[ overloadId_ ] );

	public:
		static if( type == StrawObject_MemberType.staticFunction )
		static auto call()( auto ref ParameterTypeTuple!T args ) {
			return __traits( getOverloads, This, memName_ )[ overloadId_ ]( args );
		}

		static if( type == StrawObject_MemberType.dynamicFunction )
		static auto call()( auto ref This instance, auto ref ParameterTypeTuple!T args ) {
			return __traits( getOverloads, instance, memName_ )[ overloadId_ ]( args );
		}

	}
	final static abstract class __STRAW_OBJECT_MEMBER_FIELD_DATA( string memName_ ) {

	public:
		enum type = __traits( compiles, &__traits( getMember, This, memName_ ) ) ? StrawObject_MemberType.staticField : ( __traits( compiles, &__traits( getMember, lvalueOf!This, memName_ ) ) ? StrawObject_MemberType.dynamicField : StrawObject_MemberType.enumField );
		enum name = memName_;
		alias attributes = TypeTuple!( __traits( getAttributes, __traits( getMember, This, memName_ ) ) );

	public:
		alias T = typeof( __traits( getMember, This, memName_ ) );

	public:
		static if( type == StrawObject_MemberType.dynamicField )
		ref auto getRef()( auto ref This instance ) {
			return __traits( getMember, instance, memName_ );
		}

		static if( type == StrawObject_MemberType.staticField )
		ref auto getRef()() {
			return __traits( getMember, This, memName_ );
		}

	}

}

version( unittest ) {

	struct attr {
		enum ATTR = true;
	}
	struct attr2 {
		enum ATTR2 = true;
	}
	
	class C {
		mixin( StrawObject );
		
	public:
		@attr int a;
		@attr2 int b;
		int c;
		int d;
		
	}
	
	static assert( MembersInfo_AttributeFilter!( C, MIAF_HasMember!"ATTR", true ).length == 1 );
	static assert( MembersInfo_AttributeFilter!( C, MIAF_HasMember!"ATTR2", true ).length == 1 );

}