module straw.core.object.object;

import std.traits;
import straw.core.object.traits;

/** !!! USE IT LIKE mixin( StrawObject )
 * Mixin this in any class/struct/interface and see the magic happen.
 * If you mixin it in a class/interface, make sure it is also mixed in all the ancestors and children
 * */
enum string StrawObject = "mixin StrawObjectImpl;";

/** !!! USE IT LIKE mixin( StrawObjectLite )
 * Mixin this in any class/struct/interface and see the magic happen.
 * Lite version of StrawObject - without all the heavy stuff.
 * */
enum string StrawObjectLite = "mixin StrawObjectLiteImpl;";

/** !! Du not use this, use StrawObjectLite
 * */
mixin template StrawObjectLiteImpl() {

public:
	/// I just need this. Writing typeof( this ) is ugly
	alias This = typeof( this );

}

/** !! Du not use this, use StrawObject
 * */
mixin template StrawObjectImpl() {
	mixin StrawObjectLiteImpl;

public:
	// region IS_STRAW_OBJECT stuff
public:
	/// Information for finding out if StrawObject was mixed in, but it might be only in class' or interface's ancestor
	enum IS_STRAW_OBJECT = true;
	
	/// Information for finding out if StrawObject was mixed in in the particular object
	mixin( "enum IS_STRAW_OBJECT__" ~ This.mangleof ~ "__ = true;" );

	// endregion

	// region Safety checks
private:
	/// Type check
	static assert( ( is( This == class ) || is( This == interface ) || is( This == struct ) ), "StrawObject: StrawObject can be mixed in only to classes, structs and interfaces" );
	
	/// This code checks, if all the ancestors have StrawObject mixed in. If not, throws an error
	static if( !is( This == struct ) )
	mixin( {
			import std.traits;
			foreach( Ancestor; BaseTypeTuple!This )
				static assert( isStrawObject!Ancestor || is( Ancestor == Object ), "StrawObject: StrawObject was mixed into " ~ fullyQualifiedName!This ~ " but not into " ~ fullyQualifiedName!Ancestor ~ ", which is it's ancestor" );
			
			return null;
		} () );

	// endregion

	// region Mixins
private:
	import straw.core.object.traits;

public:
	mixin StrawObject_traits;

	// endregion

}

bool isStrawObject( T )() {
	enum result = __traits( compiles, mixin( "T.IS_STRAW_OBJECT__" ~ T.mangleof ~ "__" ) );

	static assert( result || !__traits( compiles, T.IS_STRAW_OBJECT ), "StrawObject: StrawObject was mixed in one of ancestors of " ~ fullyQualifiedName!T ~ ", but not to the class/interface itself." );

	return result;
}