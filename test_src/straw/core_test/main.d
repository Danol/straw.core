module straw.core_test.main;

import straw.core.object;
import straw.core_test.a;

class A {
	mixin( StrawObject );

public:
	void c() {

	}

}

class C : A {
	mixin( StrawObject );

	int a;
	int b;
	static int x;
	enum y = 3;

	override void c() {

	}
	void c( int ) {

	}

	void d() {

	}

	static void f() {
		import std.stdio;
		writeln( "stdiotest" );
	}

	enum e = 2;

}

void ax() {
	import std.conv;
	/*foreach( mem; MembersInfo!C ) {
		//pragma( msg, "mem: ", mem );
		pragma( msg, "name: ", mem.name, " type: ", __traits( compiles, mem.type ) ? mem.type.to!string : "", " T: ", mem.T );
	}*/
}

void main() {

}